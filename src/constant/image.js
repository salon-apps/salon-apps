const data = {
    icon: require('../../assets/images/icon.png'),
    iconWhite: require('../../assets/images/icon_white.png'),
    slide1: require('../../assets/images/dummy/slide1.png'),
    slide2: require('../../assets/images/dummy/slide2.png'),
    slide3: require('../../assets/images/dummy/slide3.png'),
    slide4: require('../../assets/images/dummy/slide4.png'),
    slide5: require('../../assets/images/dummy/slide5.png'),
    hairTreatment: require('../../assets/images/driyer.png'),
    nailTreatment: require('../../assets/images/nail.png'),
    facialTreatment: require('../../assets/images/facial.png'),
    makeUp: require('../../assets/images/makeup.png'),
    hairColoring: require('../../assets/images/coloring.png'),
    creambath: require('../../assets/images/shower.png'),
    smoothing: require('../../assets/images/wig.png'),
    curly: require('../../assets/images/curly.png'),
    hairStyle: require('../../assets/images/hairStyle.png'),
    haircut: require('../../assets/images/haircut.png'),
    spa: require('../../assets/images/spa.png'),
}
export default data;

export const imageCategory = (id) => {

    switch (Number(id)) {
        case 1:
            return data.hairTreatment
        case 2:
            return data.nailTreatment
        case 3:
            return data.facialTreatment
        case 4:
            return data.makeUp
        default:
            return data.hairTreatment
    }
}

export const imageSubCategory = (id) => {

    switch (Number(id)) {
        case 1:
            return data.hairColoring
        case 2:
            return data.hairTreatment
        case 3:
            return data.curly
        case 4:
            return data.creambath
        case 5:
            return data.creambath
        case 6:
            return data.creambath
        case 7:
            return data.nailTreatment
        case 8:
            return data.nailTreatment
        case 9:
            return data.facialTreatment
        case 10:
            return data.spa
        case 11:
            return data.smoothing
        case 12:
            return data.hairStyle
        case 13:
            return data.haircut
        case 14:
            return data.makeUp
        case 15:
            return data.makeUp        
        default:
            return data.hairTreatment
    }
}