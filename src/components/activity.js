import React, { useEffect } from 'react';
import * as RootNavigation from '../router/root';
import { colors } from '../styles';
import { useTracked } from '../service';
import { ActivityIndicator } from 'react-native-paper';
import { View, Text } from 'react-native';
import SnackBar from 'react-native-snackbar-component';
import { api } from '../service/api';

import NotificationSounds, { playSampleSound } from 'react-native-notification-sounds';
import { showMessage } from "react-native-flash-message";
import { Alert } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { GoogleSignin } from '@react-native-community/google-signin';
import { key } from '../constant/key';

export const Activity = (props) => {
    const [state, action] = useTracked();

    const snackbarActive = state.alert.active;
    const snackbarType = state.alert.snackbarType;
    const snackbarMessage = state.alert.message;
    const loading = state.loading;


    // main Activity

    function configureGoogleSign() {
        GoogleSignin.configure({
            webClientId: key.webClientID,
            offlineAccess: true
        });
    }

    function backgroundFCM() {
        messaging().onNotificationOpenedApp(remoteMessage => {
            doNotif(remoteMessage)
        });

        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    doNotif(remoteMessage)
                }
            });
    }

    function foregroundFCM() {
        const unsubscribe = messaging().onMessage(async remoteMessage => {
            doNotif(remoteMessage)
        });

        return unsubscribe;
    }

    function configureFCM() {
        backgroundFCM()
        foregroundFCM()
    }

    function doNotif(data) {
        NotificationSounds.getNotifications().then(soundsList => {
            playSampleSound(soundsList[0]);
        });
        showMessage({
            message: data.notification.title,
            description: data.notification.body,
            onPress: () => {
                RootNavigation.navigate(data.data.page)
            }
        })
        notifAction(data.data)
    }

    async function notifAction({ page, params }) {
        if (page === 'TabOrder') {
            await getDataOrder()
        } else if (page === 'TabRating'){
            if(params){
                RootNavigation.navigate('Rating', {                    
                    idOrder: params
                });
            }
        }
    }

    async function getDataOrder() {
        try {
            let dataHistory = []
            let newData = []
            const request = await api('history')
            if (request) {
                if (request.data.length !== 0) {
                    for (let i in request.data) {
                        if (Number(request.data[i].idStatus) >= 5) {
                            dataHistory.push(request.data[i])
                        } else {
                            newData.push(request.data[i])
                        }
                    }
                    action({
                        type: 'setHistory',
                        order: newData,
                        history: dataHistory
                    })
                }
            }
        } catch (error) {
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    useEffect(() => {
        configureFCM();
        configureGoogleSign();
    }, []);

    // end main Activity

    return (
        <>
            <SnackBar
                visible={snackbarActive}
                textMessage={snackbarMessage}
                actionHandler={() => {
                    action({ type: 'closeAlert' })
                }}
                backgroundColor={snackbarType === 'Error' ? colors.danger : snackbarType === 'Warning' ? colors.warning : colors.primary}
                messageColor={colors.light}
                position={'bottom'}
                actionText="Ok"
            />
            {loading ? (
                <View style={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    top: 0,
                    left: 0,
                    zIndex: 1,
                    backgroundColor: 'rgba(0,0,0,0.5)'
                }}>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignContent: 'center'
                    }}>
                        <ActivityIndicator animating={loading} color={'#D6463A'} size={40} />
                    </View>
                </View>
            ) : (
                    null
                )}
        </>
    );
};