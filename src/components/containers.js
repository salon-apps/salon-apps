import React from 'react';
import {
    SafeAreaView,
    StatusBar,
    View,
    Text
} from 'react-native';
import { Appbar } from 'react-native-paper';
import { colors } from '../styles';
import textStyle from '../styles/text';

export const Space = (props) => {
    return (
        <View style={{
            height: props.size,
            width: '100%'
        }} />
    );
};

export const MainContainer = (props) => {
    return (
        <>
            <StatusBar barStyle={
                props.dark ? "dark-content" : "light-content"
            } backgroundColor={
                props.barColor ? props.barColor : '#FFFFFF'
            } />
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: colors.light
            }}>
                {props.children}
            </SafeAreaView>
        </>
    );
};

export const TabsContainer = (props) => {
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: colors.light
        }}>
            {props.children}
        </SafeAreaView>
    );
};

export const MainHeaderContainer = (props) => {
    const navigation = props.nav
    return (
        <>
            <StatusBar barStyle={
                props.dark ? "dark-content" : "light-content"
            } backgroundColor={
                props.barColor ? props.barColor : '#FFFFFF'
            } />
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: colors.light,
            }}>
                <View style={{
                    zIndex: 1
                }}>
                    <Appbar.Header style={{
                        backgroundColor: props.barColor ? props.barColor : '#FFFFFF'
                    }}>
                        {props.back && navigation !== undefined ? (
                            <Appbar.BackAction
                                onPress={() => {
                                    navigation.goBack()
                                }}
                            />
                        ) : null}
                        <Appbar.Content
                            title={props.title ? props.title : 'Title Not Found'}
                            subtitle={props.subtitle ? props.subtitle : null}
                        />
                        {/* <Appbar.Action icon="magnify"
                            onPress={() => {
                                alert('sip')
                            }} /> */}
                    </Appbar.Header>
                </View>
                {props.children}
            </SafeAreaView>
        </>
    );
};

export const DetailLayout = (props) => {

    return (
        <View style={{
            backgroundColor: 'rgba(255,255,255 ,0.9)',
            padding: 10
        }}>
            {props.title ? (
                <Text style={textStyle.title}>{props.title}</Text>
            ) : null}
            {props.children}
        </View>
    )
}