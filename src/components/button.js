import React from 'react';
import {
    View,
    Keyboard,
    Text
} from 'react-native';
import { Button, IconButton } from 'react-native-paper';
import font from '../constant/font';
import { colors } from '../styles';
import textStyle from '../styles/text';
import { str } from '../constant/string';
import { formatNumber } from '../helper/validator';
import { Space } from './containers';

function defaultAction() {
    Keyboard.dismiss()
    alert('pressed')
}

export const LoginButton = (props) => {

    const onPress = props.onPress ? props.onPress : defaultAction;
    const color = props.color ? props.color : colors.primary;
    const colorLable = props.colorLable ? props.colorLable : colors.light;
    const lable = props.lable ? props.lable : 'lable undefined'

    return (
        <View style={{
            paddingHorizontal: 20,
            paddingVertical: 10
        }}>
            <Button
                mode="contained"
                color={color}
                onPress={onPress}
                style={{
                    borderRadius: 100,
                }}
                labelStyle={{
                    fontFamily: font.primaryBold,
                    textTransform: 'capitalize',
                    color: colorLable
                }}
            >
                {lable}
            </Button>
        </View>
    );
};

export const GFLogin = (props) => {

    return (
        <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 15
        }}>
            <View style={{
                padding: 5,
                flex: 1
            }}>
                <Button
                    mode="contained"
                    icon={'google'}
                    color={'#db4a39'}
                    onPress={
                        props.GAction
                    }
                    style={{
                        borderRadius: 100,
                        padding: 4
                    }}
                >
                    login / register
                </Button>
            </View>
            {/* <View style={{
                padding: 5,
                flex: 1
            }}>
                <Button
                    mode="contained"
                    icon={'facebook'}
                    color={'#3F51B5'}
                    onPress={
                        props.FAction
                    }
                    style={{
                        borderRadius: 100,
                        padding: 4
                    }}
                >
                    login
                </Button>
            </View> */}
        </View>
    );
};

export const SFLogin = (props) => {

    return (
        <View style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 15,
            paddingTop: 10
        }}>
            <View style={{
                flex: 1
            }}>
                <Button
                    mode="text"
                    color={'#3F51B5'}
                    onPress={
                        props.forgot
                    }
                    labelStyle={textStyle.signUp}
                >
                    {str.forgotPass}
                </Button>
            </View>
        </View>
    );
};

export const TNCButton = (props) => {
    const onCheck = props.onCheck;
    const check = props.check;

    return (
        <View style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <IconButton
                icon={check ? 'checkbox-marked-outline' : 'checkbox-blank-outline'}
                color={check ? colors.primary : colors.grey}
                size={30}
                animated
                onPress={onCheck}
            />

            <Button
                mode="text"
                onPress={() => {
                    alert('pressed')
                }}
                labelStyle={textStyle.tnc}
            >
                {str.tnc}
            </Button>
        </View>
    );
};

export const ListPrice = (props) => {
    const onCheck = props.onCheck;
    const check = props.check;

    return (
        <View style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 5
        }}>
            <View style={{
                padding: 10,
            }}>
                <Text style={textStyle.title}>{props.title}</Text>
                <Text style={textStyle.price}>{formatNumber(props.price)}</Text>
            </View>
            <IconButton
                icon={check ? 'checkbox-marked-outline' : 'checkbox-blank-outline'}
                color={check ? colors.primary : colors.grey}
                size={30}
                animated
                onPress={onCheck}
            />
        </View>
    );
};