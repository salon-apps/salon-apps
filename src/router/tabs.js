import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Home from '../view/menu/home';
import Order from '../view/menu/order';
import Help from '../view/menu/help';
import { View, StatusBar, TouchableOpacity, Dimensions, Text } from 'react-native';
import { colors } from '../styles';
import containerStyke from '../styles/container';
import Profile from '../view/menu/profile';
import { waLinking } from '../helper/whatsapp';
import { key } from '../constant/key';

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {

  const height = Dimensions.get('screen').height;
  const width = Dimensions.get('screen').width;

  const [bar, setBar] = useState(true)

  return (
    <>
      <StatusBar
        barStyle={"light-content"}
        backgroundColor={bar ? colors.primaryD : colors.secondaryD}
      />
      <View style={{
        position: 'absolute',
        bottom: height * 0.05,
        left: width * 0.4,
        borderRadius: 100,
        zIndex: 2,
        backgroundColor: bar ? colors.primary : colors.secondary
      }}>
        <TouchableOpacity style={{
          height: width * 0.2,
          width: width * 0.2,
          // backgroundColor: bar ? colors.primaryD : colors.secondaryD,
          backgroundColor: '#00A859',
          borderRadius: 50,
          borderWidth: 5,
          borderColor: colors.light,
          alignItems: 'center',
          justifyContent: 'center'
        }} onPress={()=> {
          waLinking('Hai Jelita Salon & Spa Homeservices ', key.jelitaPhone)
        }}>
          <Icon
            name="logo-whatsapp"
            size={40}
            color={'#FFF'}
          />
        </TouchableOpacity>
      </View>
      <Tab.Navigator
        activeColor={
          colors.light
        }
        shifting={true}
        barStyle={{
          backgroundColor: colors.secondary
        }}
      >
        <Tab.Screen
          name="TabHome"
          component={Home}
          listeners={{
            tabPress: e => {
              setBar(true)
            },
          }}
          options={{
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-home"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.primaryD,
          }}
        />
        <Tab.Screen
          name="TabOrder"
          component={Order}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Pesanan Ku',
            tabBarIcon: () => {
              return (
                <Icon
                  name="md-basket"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.secondaryD
          }}
        />
        <Tab.Screen
          name="TabHelp"
          component={Help}
          listeners={{
            tabPress: e => {
              setBar(true)
            },
          }}
          options={{
            tabBarLabel: 'Bantuan',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-help-circle"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.primaryD
          }}
        />
        <Tab.Screen
          name="TabProfile"
          component={Profile}
          listeners={{
            tabPress: e => {
              setBar(false)
            },
          }}
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: () => {
              return (
                <Icon
                  name="ios-person"
                  size={30}
                  style={containerStyke.tabsIcon}
                  color={colors.light}
                />
              )
            },
            tabBarColor: colors.secondaryD
          }}
        />
      </Tab.Navigator>
    </>
  );
}

export default MyTabs;