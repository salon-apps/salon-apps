import React from 'react';
import { navigationRef } from './root';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import Login from '../view/auth/login';
import Intro from '../view/auth/intro';
import MyTabs from './tabs';
// import Register from '../view/auth/register';
import Forgot from '../view/auth/forgot';
import Category from '../view/page/category';
import Details from '../view/page/details';
import Order from '../view/page/order';
import Location from '../view/page/location';
import DetailOrder from '../view/page/detailOrder';
import ChatPage from '../view/page/chat';
import RatingPage from '../view/page/rating';

const Stack = createStackNavigator();

const MainRoot = () => {

    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          initialRouteName={'Login'}
          headerMode="none"
        >
          <Stack.Screen name="Login" component={Intro} />
          <Stack.Screen name="Tabs" component={MyTabs} />
          <Stack.Screen name="Forgot" component={Forgot} />
          <Stack.Screen name="Category" component={Category} />
          <Stack.Screen name="Details" component={Details} />
          <Stack.Screen name="Order" component={Order} />
          <Stack.Screen name="DetailOrder" component={DetailOrder} />
          <Stack.Screen name="SelectLoc" component={Location} />
          <Stack.Screen name="ChatScreen" component={ChatPage} />
          <Stack.Screen name="Rating" component={RatingPage} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  
}

export default MainRoot;