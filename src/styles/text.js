import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from '.';
import font from '../constant/font';

const styles = StyleSheet.create({
    welcome: {
        fontFamily: font.primaryBold,
        fontSize: 25,
        lineHeight: 25,
        textTransform: 'capitalize'
    },
    welcomeLight: {
        fontFamily: font.primaryBold,
        fontSize: 25,
        lineHeight: 30,
        color: colors.light,
        textTransform: 'capitalize'
    },
    subWelcomeLight: {
        fontFamily: font.primaryBold,
        fontSize: 15,
        lineHeight: 15,
        color: colors.light,
        textTransform: 'capitalize'
    },
    // text general
    title: {
        fontFamily: font.primaryBold,
        fontSize: 15,
        color: colors.dark,
        textTransform: 'capitalize'     
    },
    price: {
        fontFamily: font.primaryBold,
        fontSize: 18,
        color: colors.primary,
        textTransform: 'capitalize'     
    },
    subTitle: {
        fontFamily: font.primaryBold,
        fontSize: 10,
        color: colors.dark        
    },
    content: {
        fontFamily: font.primary,
        fontSize: 10,
        color: colors.dark        
    },
    // content button
    signUp: {
        fontSize: 10,
        textDecorationLine: 'underline',
        color: colors.grey,
        fontFamily: font.primaryBold,
        textTransform: 'capitalize',
    },
    tnc: {
        fontSize: 12,
        padding: 0,
        margin: 0,
        color: colors.dark,
        fontFamily: font.primaryBold,
        textTransform: 'capitalize',
    },
    // input form
    titleInput: {
        padding: 0,
        margin: 0,
        fontFamily: font.primaryBold,
        color: colors.grey,
    },
    // sparator
    sparatorText: {
        padding: 0,
        margin: 0,
        alignSelf: 'center',
        fontFamily: font.primary,
        color: colors.grey
    }
});

export default styles;