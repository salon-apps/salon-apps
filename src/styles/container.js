import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from '.';
import font from '../constant/font';

const styles = StyleSheet.create({
    card: {
        paddingHorizontal: 10,
        padding: 10,
        margin: 10,
        borderRadius: 20,
        backgroundColor: colors.lightL,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2
    },
    tabsIcon: {
        width: 30,
        padding: 2,
        marginTop: -7
    },
    listArticle: {
        padding: 10,
        borderRadius: 15,
        marginVertical: 50,
        marginHorizontal: 10,
        backgroundColor: colors.light
    },
    list: {
        padding: 10,
        borderRadius: 15,
        marginVertical: 50,
        marginHorizontal: 10,
    },
    flexBetwen: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    flexCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexRight: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
});

export default styles;