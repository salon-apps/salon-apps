import React from 'react';
import { StyleSheet } from 'react-native';
import { colors } from '.';
import font from '../constant/font';

const styles = StyleSheet.create({
    absoluteRight: {
        position: 'absolute',
        right: 10,
        bottom: 0
    }
});

export default styles;