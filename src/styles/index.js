export const colors = 
    {
        primary: '#E91E63',
        primaryD: '#880E4F',
        primaryL: '#FCE4EC',
        secondary: '#2196F3',
        secondaryD: '#0D47A1',
        danger: '#f44336',
        warning: '#FBC02D',
        warningD: '#F57F17',
        whatsapp: '#00A859',
        greyL: '#E0E0E0',
        grey: '#616161',
        light: '#fafafa',
        lightL: '#FFFFFF',
        dark: '#000000',
    }
