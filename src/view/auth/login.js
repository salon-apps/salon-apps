import React, { useEffect, useState } from 'react';
import messaging from '@react-native-firebase/messaging';
import { getUniqueId } from 'react-native-device-info';
import { useTracked } from '../../service';
import { MainContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { api } from '../../service/api';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { getProfile, jwtVerify } from '../../helper/enct';
import {
    // LoginButton, 
    GFLogin,
    // SFLogin 
} from '../../components/button';
// import { str } from '../../constant/string';
// import containerStyle from '../../styles/container';
// import { Input } from '../../components/input';
// import { emailValidator } from '../../helper/validator';
// import { jwtVerify, encriptor } from '../../helper/enct';

const Login = ({ navigation }) => {
    const [state, action] = useTracked();
    const [render, setRender] = useState(false);

    // const [email, setEmail] = useState('');
    // const [pass, setPass] = useState('');
    // const [emailKeybord, setEmailKeybord] = useState('email-address');

    async function updateDeviceID(data) {
        await api('updateDevice', data)
        const userProfile = await getProfile();
        setRender(true)
        if (userProfile.idUser) {
            action({
                type: 'updateProfile',
                data: userProfile
            })
            navigation.reset({
                index: 0,
                routes: [{ name: 'Tabs' }],
            });
        }        
    }

    function getTokenFCM(ID) {
        // Get the device token        
        messaging()
            .getToken()
            .then(token => {
                console.log(token)
                updateDeviceID({
                    deviceID: ID,
                    firebaseToken: token
                })
            });

        // Listen to whether the token changes
        return messaging().onTokenRefresh(token => {
            alert('token refresh')
            updateDeviceID({
                deviceID: ID,
                firebaseToken: token
            })
        });
    }

    function getDeviceID() {
        const ID = getUniqueId()
        if (ID) {
            getTokenFCM(ID);
        } else {
            // setRender(true)
            alert('Device Tidak Didukung')
        }
    }

    async function auth() {
        const user = await AsyncStorage.getItem('token')
        if (user !== null) {
            getDeviceID()
        } else {
            setRender(true)
        }
    }

    useEffect(() => {
        auth()
    }, [])

    async function googleReset() {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
        } catch (error) {
            console.error(error);
        }
    };

    async function loginProsess(data) {

        async function storeToken() {
            const token = data.data
            await AsyncStorage.setItem('token', token)            
            const userProfile = await jwtVerify(token);
            if (userProfile.payload.idUser) {
                action({
                    type: 'updateProfile',
                    data: userProfile.payload
                })
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Tabs' }],
                });
            }            
            action({ type: 'loadStop' })
            await googleReset()
        }

        if (data.error) {
            action({ type: 'loadStop' });
            if (data.errorCode === 'unregistered') {
                action({
                    type: 'errorAlert',
                    snackbarType: 'success',
                    message: 'Register User Berhasil'
                })
                storeToken()
            } else {
                action({ type: 'errorAlert', message: data.message })
            }
        } else {
            storeToken()
        }
    }

    // async function emailLogin() {
    //     try {
    //         action({ type: 'loadStart' })
    //         const data = await api('login', {
    //             user: email,
    //             pass: pass.length < 5 ? 'sempak' : encriptor(pass)
    //         })
    //         loginProsess(data)
    //     } catch (error) {
    //         action({ type: 'loadStop' })
    //         action({ type: 'errorAlert', message: error.message })
    //         console.log(error)
    //     }
    // }

    async function googleSignIn() {
        try {
            action({ type: 'loadStart' })
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const data = await api('loginGoogle', {
                tokenID: userInfo.idToken
            })
            loginProsess(data)
        } catch (error) {
            action({ type: 'loadStop' })
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('user cancelled the login flow')
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('operation (e.g. sign in) is in progress already')
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                alert('play services not available or outdated')
            } else {
                action({ type: 'errorAlert', message: error.message })
                console.log(error)
            }
        }
    };

    //// "errorCode": "unregistered",
    // function doLogin() {
    //     if (email.toString().substring(0, 2) === '08' || email.toString().substring(0, 3) === '628') {
    //         emailLogin()
    //     } else {
    //         if (!emailValidator(email)) {
    //             action({ type: 'errorAlert', message: 'Format Email Salah' })
    //         } else {
    //             emailLogin()
    //         }
    //     }
    // }

    if (render) {
        return (
            <MainContainer dark={false} barColor={colors.primaryD}>
                <MainBG />
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    contentContainerStyle={{
                        flexGrow: 1,
                        justifyContent: 'space-between',
                        flexDirection: 'column',
                        background: 'transparent'
                    }}
                >
                    <View style={{
                        padding: 20,
                        marginTop: 20
                    }}>
                        <Text style={textStyle.subWelcomeLight}>{'selamat datang di'}</Text>
                        <Text style={textStyle.welcomeLight}>{'cantik home services'}</Text>
                    </View>
                    <View style={{
                        marginBottom: 10
                    }}>
                        <Image
                            style={{
                                margin: 40,
                                width: 100,
                                height: 100,
                                resizeMode: 'contain',
                                alignSelf: 'center'
                            }} source={require('../../../assets/images/icon.png')} />
                        <GFLogin
                            GAction={() => {
                                googleSignIn()
                            }} FAction={() => {
                                alert('Coming Soon')
                            }} />
                    </View>
                </ScrollView>
            </MainContainer>
        )
    } else {
        return (
            <View style={{
                height: '100%',
                width: '100%',
                backgroundColor: colors.primary
            }} />
        )
    }
};

export default Login;
