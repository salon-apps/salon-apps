import React, { useState } from 'react';
import { MainContainer, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import {
    str
} from '../../constant/string';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { Input } from '../../components/input';
import { LoginButton, TNCButton } from '../../components/button';

const ForgotPass = ({ navigation }) => {
    const [email, setEmail] = useState('');

    return (
        <MainContainer dark={false} barColor={colors.secondary}>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    padding: 20
                }}>
                    <Text style={textStyle.welcomeLight}>{str.forgotPass}</Text>
                </View>
                <View>
                <View style={containerStyle.card}>
                <Space size={10} />
                <Text style={[textStyle.title, {
                    textAlign: 'center'
                }]}>Masukan Email</Text>
                <Space size={20} />
                    <Input
                        title={'Email'}
                        keyboardType={'email-address'}
                        value={email}
                        action={text => {
                            setEmail(text)
                        }}
                    />                   
                </View>
                <View>                    
                    <LoginButton lable={'lupa password'} />
                </View>
                </View>
            </ScrollView>
        </MainContainer>
    );
};

export default ForgotPass;
