import React, { useState } from 'react';
import { MainContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import {
    str
} from '../../constant/string';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { Input } from '../../components/input';
import { LoginButton, TNCButton } from '../../components/button';

const Login = ({ navigation }) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [pass, setPass] = useState('');
    const [cPass, setCPass] = useState('');

    const [check, setCheck] = useState(false);

    return (
        <MainContainer dark={false} barColor={colors.primary}>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    padding: 20
                }}>
                    <Text style={textStyle.welcomeLight}>{str.register}</Text>
                    <Text style={textStyle.subWelcomeLight}>{str.account}</Text>
                </View>
                <View style={containerStyle.card}>
                    <Input
                        title={str.name}
                        value={name}
                        action={text => {
                            setName(text)
                        }}
                    />
                    <Input
                        title={'Email'}
                        keyboardType={'email-address'}
                        value={email}
                        action={text => {
                            setEmail(text)
                        }}
                    />
                    <Input
                        title={'Phone'}
                        keyboardType={'phone-pad'}
                        value={phone}
                        action={text => {
                            setPhone(text)
                        }}
                    />
                    <Input
                        title={str.password}
                        value={pass}
                        type={'password'}
                        action={text => setPass(text)}
                    />
                    <Input
                        title={str.passConfirm}
                        value={cPass}
                        type={'password'}
                        action={text => setCPass(text)}
                    />
                </View>
                <View>
                    <TNCButton check={check} onCheck={() => {
                        setCheck(!check)
                    }} />
                    <LoginButton lable={str.register} />
                </View>
            </ScrollView>
        </MainContainer>
    );
};

export default Login;
