import React, { useState, useEffect } from 'react';
import { useTracked } from '../../service';
import { Space, MainHeaderContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { Button } from 'react-native-paper';
import { MainBG } from '../../components/abstrack';
import { str } from '../../constant/string';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import textStyle from '../../styles/text';
import { CardMenu, ListArticle } from '../../components/card';
import image from '../../constant/image';
import { Banner } from '../../components/banner';
import { ListPrice } from '../../components/button';
import { formatNumber } from '../../helper/validator';

const Details = ({ navigation, route: {
    params: {
        title,
        payload,
        image
    }
} }) => {

    const [state, action] = useTracked();

    const [check, setCheck] = useState('');
    const [price, setPrice] = useState(0);
    const [addOn, setAddOn] = useState([])
    const [addOncheck, setAddOnCheck] = useState('');
    const [addOnPrice, setAddOnPrice] = useState(0);

    // useEffect(() => {
    //     alert(image)
    // }, []);

    return (
        <MainHeaderContainer
            dark={false}
            back
            barColor={colors.primaryD}
            title={title}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View>
                    <Image
                        style={{
                            height: 200,
                            width: '100%',
                            resizeMode: 'cover',
                            alignSelf: 'center'
                        }} source={{
                            uri: image
                        }} />
                    <DetailLayout
                        title={
                            'Deskripsi'
                        }>
                        <Text style={textStyle.content}>{
                            payload.descProduct
                        }</Text>
                    </DetailLayout>
                    <DetailLayout title={
                        payload.displayPrice
                    }>
                        {
                            payload.detailProduct.map((row, i) => (
                                <View key={i}>
                                    <ListPrice
                                        check={row.idDetailProduct === check.idDetailProduct}
                                        title={row.nameDetailProduct}
                                        price={row.priceDetailProduct}
                                        onCheck={() => {
                                            row.title = title
                                            setCheck(row)
                                            setPrice(row.priceDetailProduct)
                                            let newAddOn = payload.addOn.filter(function (rows) {
                                                return rows.idDetailProduct === row.idDetailProduct
                                            });
                                            setAddOnCheck('')
                                            setAddOnPrice(0)
                                            setAddOn(newAddOn)
                                        }} />
                                </View>
                            ))
                        }
                    </DetailLayout>
                    {
                        addOn.length !== 0 ? (
                            <DetailLayout title={
                                'Tambahan Lainya'
                            }>
                                {
                                    addOn.map((row, i) => (
                                        <View key={i}>
                                            <ListPrice
                                                check={row.idAddOn === addOncheck.idAddOn}
                                                title={row.nameAddOn}
                                                price={row.priceAddOn}
                                                onCheck={() => {
                                                    if (addOncheck.idAddOn === row.idAddOn) {
                                                        setAddOnCheck('')
                                                        setAddOnPrice(0)
                                                    } else {
                                                        row.title = title
                                                        setAddOnCheck(row)
                                                        setAddOnPrice(row.priceAddOn)
                                                    }
                                                }} />
                                        </View>
                                    ))
                                }
                            </DetailLayout>
                        ) : (
                            null
                        )
                    }
                </View>
            </ScrollView>
            <View style={{
                padding: 20,
                height: 70,
                backgroundColor: colors.primaryD
            }}>
                <View style={containerStyle.flexBetwen}>
                    <View>
                        <Text style={[
                            textStyle.title,
                            {
                                color: colors.light
                            }
                        ]}>Total Harga</Text>
                        <Text style={[
                            textStyle.price,
                            {
                                color: colors.light
                            }
                        ]}>{formatNumber(price + addOnPrice)}</Text>
                    </View>
                    <Button
                        icon="arrow-right-bold"
                        mode="contained"
                        color={colors.secondary}
                        onPress={() => {
                            if (price < 100) {
                                action({ type: 'errorAlert', message: payload.displayPrice + ' Tidak Boleh Kosong' })
                            } else {
                                navigation.navigate('Order', {
                                    payload: {
                                        product: check,
                                        addOn: addOncheck
                                    }
                                });
                            }
                        }}>
                        Lanjut
                    </Button>
                </View>
            </View>
        </MainHeaderContainer>
    );
};

export default Details;
