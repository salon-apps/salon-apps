import React, { useEffect } from 'react';
import moment from 'moment';
import { MainHeaderContainer, DetailLayout, Space } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image
} from 'react-native';
import MapView from 'react-native-maps';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { formatNumber } from '../../helper/validator';
import { RatingComponent } from '../../components/card';

const DetailOrder = ({ navigation, route: {
    params: {
        data
    }
} }) => {

    useEffect(()=> {
        console.log(data);
    }, [data])

    return (
        <MainHeaderContainer
            title={'Detail Riwayat Pesanan'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Detail Partner Kami'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        {
                            data.partnerImage ? (
                                <Image
                                    style={{
                                        height: 70,
                                        width: 70,
                                        marginRight: 20,
                                        marginTop: 20,
                                        padding: 10,
                                        borderRadius: 100,
                                        resizeMode: 'contain',
                                        alignSelf: 'center'
                                    }} source={{ uri: data.partnerImage }} />
                            ) : (
                                    <View
                                        style={{
                                            height: 70,
                                            width: 70,
                                            marginRight: 20,
                                            marginTop: 20,
                                            padding: 10,
                                            backgroundColor: colors.greyL,
                                            borderRadius: 100,
                                            resizeMode: 'contain',
                                            alignSelf: 'center'
                                        }} />
                                )
                        }
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Text numberOfLines={1} style={[textStyle.welcome, {
                                color: colors.primary
                            }]}>{data.namaLengkap}</Text>
                            <Text numberOfLines={1} style={[textStyle.title, {
                                color: colors.dark
                            }]}>{data.alamatLengkap}</Text>
                        </View>
                    </View>
                </DetailLayout>
                <RatingComponent value={data.rating} idOrder={data.idOrder} />
                <DetailLayout
                    title={
                        'Lokasi'
                    }>
                    <Text style={textStyle.content}>
                        {data.detailLoc.display}
                    </Text>
                </DetailLayout>
                <View style={{
                    height: 150,
                    width: '100%',
                    backgroundColor: colors.grey
                }}>
                    <MapView
                        style={{
                            flex: 1
                        }}
                        initialRegion={{
                            latitude: data.detailLoc.latitude,
                            longitude: data.detailLoc.longitude,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01
                        }}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: data.detailLoc.latitude,
                                longitude: data.detailLoc.longitude
                            }}
                            draggable={false}
                        />
                    </MapView>
                </View>
                <View style={{
                    padding: 20,
                    backgroundColor: colors.lightL
                }}>
                    <DetailLayout
                        title={
                            'Detail Lokasi'
                        }>
                        <Text style={textStyle.content}>
                            {data.detailLoc.detailLoc}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Catatan'
                        }>
                        <Text style={textStyle.content}>
                            {data.note}
                        </Text>
                    </DetailLayout>
                    <DetailLayout
                        title={
                            'Waktu Pemesanan'
                        }>
                        <Text style={textStyle.content}>
                            {moment(data.timeOrder).format('DD-MM-SS HH:mm:ss') + ' WIB'}
                        </Text>
                    </DetailLayout>

                </View>
                <DetailLayout
                    title={
                        'Detail Pesanan'
                    }>
                    <Space size={5} />
                    <Text style={[textStyle.title, {
                        fontSize: 12,
                        color: colors.secondary
                    }]}>
                        {data.title}
                    </Text>
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {data.nameProduct}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(data.priceDetailProduct)}
                        </Text>
                    </View>
                    {
                        data.priceAddOn ? (
                            <View style={containerStyle.flexBetwen}>
                                <Text style={[textStyle.content, {
                                    fontSize: 12,
                                    color: colors.grey
                                }]}>
                                    {data.nameAddOn}
                                </Text>
                                <Text style={[textStyle.title, {
                                    color: colors.secondary
                                }]}>
                                    {formatNumber(data.priceAddOn)}
                                </Text>
                            </View>
                        ) : (
                                null
                            )
                    }
                    <Space size={10} />
                    <View style={{
                        height: 2,
                        width: '100%',
                        backgroundColor: colors.greyL
                    }} />
                    <View style={containerStyle.flexBetwen}>
                        <Text style={[textStyle.content, {
                            fontSize: 12,
                            color: colors.grey
                        }]}>
                            {'Total'}
                        </Text>
                        <Text style={[textStyle.title, {
                            color: colors.secondary
                        }]}>
                            {formatNumber(
                                data.priceAddOn ? data.priceAddOn + data.priceDetailProduct : data.priceDetailProduct
                            )}
                        </Text>
                    </View>
                </DetailLayout>
                <Space size={20} />
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default DetailOrder;
