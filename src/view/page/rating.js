import React, { useEffect, useState } from 'react';
import { useTracked } from '../../service';
import { AirbnbRating } from 'react-native-ratings';
import { Space, MainHeaderContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    Image,
    TextInput
} from 'react-native';
import textStyle from '../../styles/text';
import containerStyle from '../../styles/container';
import { colors } from '../../styles';
import { api } from '../../service/api';
import { LoginButton } from '../../components/button';

const ratingEnum = ['Kurang Memuaskan', 'Biasa Aja', 'Bagus', 'Puas', 'Sangat Memuaskan'];

const RatingPage = ({ navigation, route: {
    params
} }) => {

    const [state, action] = useTracked();

    const [note, setNote] = useState('');
    const [rating, setRating] = useState(0);
    const [data, setData] = useState(false);

    useEffect(() => {
        let idOrder = 64;
        if (params) {
            if (params.idOrder) {
                idOrder = params.idOrder;
            }
        }
        getPartnerData(idOrder)
    }, []);

    function ratingCompleted(rating) {
        setRating(rating)
        setNote(ratingEnum[rating - 1])
    }

    async function getPartnerData(ID) {
        try {
            const response = await api('detailPartner', ID)
            if (response.data) {
                setData(response.data)
                if (data.ratingNote) {
                    setNote(data.ratingNote)
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    async function doRating(ID) {
        try {
            action({ type: 'loadStart' })
            let idOrder = 64;
            if (params) {
                if (params.idOrder) {
                    idOrder = params.idOrder;
                }
            }

            const response = await api('updateRating', {
                rating,
                ratingNote: note,
                idOrder
            })
            if(response){
                action({ type: 'loadStop' })
                if(!response.error){
                    action({ type: 'successAlert', message: 'Berhasil Mengirim Rating' })
                }
            }
            console.log(response)
        } catch (error) {
            action({ type: 'loadStop' })
            console.log(error)
        }
    }

    return (
        <MainHeaderContainer
            title={'Beri Rating Kami'}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <DetailLayout
                    title={
                        'Detail Partner Kami'
                    }>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        {
                            data.partnerImage ? (
                                <Image
                                    style={{
                                        height: 70,
                                        width: 70,
                                        marginRight: 20,
                                        marginTop: 20,
                                        padding: 10,
                                        borderRadius: 100,
                                        resizeMode: 'contain',
                                        alignSelf: 'center'
                                    }} source={{ uri: data.partnerImage }} />
                            ) : (
                                    <View
                                        style={{
                                            height: 70,
                                            width: 70,
                                            marginRight: 20,
                                            marginTop: 20,
                                            padding: 10,
                                            backgroundColor: colors.greyL,
                                            borderRadius: 100,
                                            resizeMode: 'contain',
                                            alignSelf: 'center'
                                        }} />
                                )
                        }
                        <View style={{
                            marginTop: 20,
                        }}>
                            <Text numberOfLines={1} style={[textStyle.welcome, {
                                color: colors.primary
                            }]}>{data.namaLengkap}</Text>
                            <Text numberOfLines={1} style={[textStyle.title, {
                                color: colors.dark
                            }]}>{data.alamatLengkap}</Text>
                        </View>
                    </View>
                </DetailLayout>
                <DetailLayout
                    title={
                        'Beri Rating'
                    }>
                    <AirbnbRating
                        onFinishRating={(e) => {
                            ratingCompleted(e)
                        }}
                        count={5}
                        reviews={ratingEnum}
                        defaultRating={data.rating ? data.rating : 0}
                        size={20}
                    />
                </DetailLayout>
                <DetailLayout
                    title={
                        'Komentar Pelanggan'
                    }>
                    <TextInput
                        multiline={true}
                        numberOfLines={2}
                        style={{
                            backgroundColor: colors.greyL,
                            borderRadius: 15,
                            padding: 10,
                            marginHorizontal: 5,
                            marginVertical: 20
                        }}
                        onChangeText={(text) => {
                            setNote(text)
                        }}
                        value={note} />
                </DetailLayout>
            </ScrollView>
            <View>
                <LoginButton lable={'Kirim Rating'} onPress={() => {
                    doRating();
                }} />
            </View>
        </MainHeaderContainer>
    );
};

export default RatingPage;
