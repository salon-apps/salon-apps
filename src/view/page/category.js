import React, { useEffect } from 'react';
import { useTracked } from '../../service';
import { MainHeaderContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Image
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { colors } from '../../styles';
import containerStyle from '../../styles/container';
import { ListArticle } from '../../components/card';
import imageSrc, { imageCategory, imageSubCategory } from '../../constant/image';


const Category = ({ route, navigation }) => {

    const [state, dispatch] = useTracked();

    const { title, data, image } = route.params;

    // useEffect(() => {
    //     alert(image)
    // }, []);

    function goDetails(data) {
        navigation.navigate('Details', {
            title: data.nameProduct,
            payload: data,
            image
        });
    }

    return (
        <MainHeaderContainer
            title={title}
            dark={false}
            back
            barColor={colors.primaryD}
            nav={navigation}>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <Image
                    style={{
                        marginHorizontal: 20,
                        marginVertical: 50,
                        width: 100,
                        height: 100,
                        resizeMode: 'contain',
                        alignSelf: 'center'
                    }} source={imageSrc.iconWhite} />
                <View style={containerStyle.list}>
                    {data.map((row, i) => (
                        <View key={i}>
                            <ListArticle
                                onPress={() => {
                                    goDetails(row)
                                }}
                                title={row.nameProduct}
                                content={row.descProduct}
                                icon={imageSubCategory(row.idProduct)}
                            />
                        </View>
                    ))}
                </View>
            </ScrollView>
        </MainHeaderContainer>
    );
};

export default Category;
