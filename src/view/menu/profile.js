import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Button } from 'react-native-paper'
import { useTracked } from '../../service';
import { MainContainer, Space, TabsContainer, DetailLayout } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { SecondaryBG } from '../../components/abstrack';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { getProfile } from '../../helper/enct';

const ProfileComponet = ({ navigation, render }) => {

    const [state, dispatch] = useTracked();

    // useEffect(() => {
    //     if (render) {
    //         storeProfile()
    //     }
    // }, [render]);
    
    async function logOut() {
        await AsyncStorage.removeItem('token')
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
        });
    }

    return (
        <TabsContainer>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    padding: 20
                }}>
                    <Text style={[textStyle.title, {
                        fontSize: 24,
                        color: colors.light
                    }]}>{'Profile Ku'}</Text>
                </View>
                <Space size={30} />
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <TouchableOpacity>
                        <Image source={
                            state.profile.userImage ? {
                                uri: state.profile.userImage
                            } :
                            require('../../../assets/images/default-profile.png')
                        } style={{
                            height: 100,
                            width: 100,
                            resizeMode: 'cover',
                            borderRadius: 100,
                            borderWidth: 2,
                            borderColor: colors.light
                        }} />
                    </TouchableOpacity>
                </View>
                <Space size={20} />
                <View style={{
                    backgroundColor: colors.lightL,
                    margin: 20,
                    borderRadius: 10,
                    borderWidth: 2,
                    borderColor: colors.primary
                }}>
                    <View style={{
                        padding: 10
                    }}>
                        <DetailLayout
                            title={
                                'Email'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11
                            }]}>{state.profile.email}</Text>
                        </DetailLayout>
                        <DetailLayout
                            title={
                                'Nama Lengkap'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11
                            }]}>{state.profile.fullName}</Text>
                        </DetailLayout>
                        <DetailLayout
                            title={
                                'Lokasi Terbaru'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11
                            }]}>{state.profile.location.display}</Text>
                        </DetailLayout>
                        <DetailLayout
                            title={
                                'HP'
                            }>
                            <Text style={[textStyle.content, {
                                fontSize: 11,
                                color: state.profile.hp ? colors.dark : colors.danger
                            }]}>{state.profile.hp ? state.profile.hp : 'belum terkonfirmasi'}</Text>
                        </DetailLayout>
                    </View>
                    <Button
                        mode="contained"
                        color={colors.primary}
                        style={{
                            borderRadius: 0,
                            borderBottomLeftRadius: 7,
                            borderBottomRightRadius: 7
                        }}
                        onPress={() => {
                            logOut()
                        }}
                        labelStyle={[textStyle.title, {
                            textTransform: 'capitalize',
                            color: colors.light,
                        }]}
                    >
                        logout
                    </Button>
                </View>
                <Space size={50} />
            </ScrollView>
        </TabsContainer>
    );
};

const Profile = ({ navigation }) => {

    const [render, setRender] = useState()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setRender(true)
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setRender(false)
        });

        return unsubscribe;
    }, [navigation]);

    return <ProfileComponet navigation={navigation} render={render} />
}

export default Profile;
