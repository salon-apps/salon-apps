import React, { useState, useEffect } from 'react';
import { api } from '../../../service/api';
import { useTracked } from '../../../service';
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import {
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { colors } from '../../../styles';
import textStyle from '../../../styles/text';
import OrderPage from './order';
import HistoryPage from './history';

const initialLayout = { width: Dimensions.get('window').width };

const Order = ({ navigation }) => {

    const [state, action] = useTracked();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Pesanan' },
        { key: 'second', title: 'Riwayat' },
    ]);

    useEffect(() => {             
        getData()
    }, [])

    useEffect(() => {   
        if(state.updateState){
            if(state.updateState === 'updateHistory'){
                getData();
                action({
                    type: 'updateState',
                    data: false
                })
            }
        }          
    }, [state.updateState])

    async function getData() {
        try {            
            action({
                type: 'loadStart'
            })
            let dataHistory = []
            let newData = []
            const request = await api('history')
            if (request) {        
                action({
                    type: 'loadStop'
                })        
                if (request.data.length !== 0) {
                    for (let i in request.data) {
                        if (Number(request.data[i].idStatus) >= 5) {
                            dataHistory.push(request.data[i])
                        } else {
                            newData.push(request.data[i])
                        }
                    }
                    action({
                        type: 'setHistory',
                        order: newData,
                        history: dataHistory
                    })
                }
            }
        } catch (error) {            
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    const renderScene = SceneMap({
        first: OrderPage,
        second: HistoryPage        
    });

    const _renderTabBar = (props) => {

        return (
            <View style={{
                flexDirection: 'row',
                paddingTop: 0,
            }}>
                {props.navigationState.routes.map((route, i) => {

                    return (
                        <TouchableOpacity
                            key={i}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                padding: 10,
                                backgroundColor: index === i ? colors.secondary : colors.secondaryD
                            }}
                            onPress={() => setIndex(i)}>
                            <Animated.Text style={[
                                textStyle.title,
                                {
                                    color: colors.light,
                                    fontSize: index === i ? 16 : 12
                                }
                            ]}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    return (
        <TabView
            lazy
            navigationState={{ index, routes }}
            renderScene={renderScene}
            renderTabBar={_renderTabBar}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
};

export default Order;
