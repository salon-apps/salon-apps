import React, { useState, useEffect } from 'react';
import { useTracked } from '../../../service';
import { TabsContainer, Space } from '../../../components/containers';
import {
    ScrollView, View
} from 'react-native';
import { SecondaryBG } from '../../../components/abstrack';
import { ListOrder } from '../../../components/card';
import containerStyle from '../../../styles/container';

const OrderPage = () => {

    const [state, action] = useTracked();

    return (
        <TabsContainer>
            <SecondaryBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <Space size={20} />
                {
                    state.order.map((row, i) => (
                        <View key={i}>
                            <ListOrder row={row} />
                        </View>
                    ))
                }
                <Space size={70} />
            </ScrollView>
        </TabsContainer>
    )
}

export default OrderPage;
