import React, { useEffect, useState } from 'react';
import { useTracked } from '../../service';
import { Space, TabsContainer } from '../../components/containers';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { MainBG } from '../../components/abstrack';
import { str } from '../../constant/string';
import { colors } from '../../styles';
import textStyle from '../../styles/text';
import { ListArticle } from '../../components/card';
import { imageCategory } from '../../constant/image';
import { Banner } from '../../components/banner';
import { api } from '../../service/api';

const HomeComponet = ({ navigation, render }) => {

    const [state, action] = useTracked();
    const [load, setLoad] = useState(false);
    const [data, setData] = useState([])

    useEffect(() => {
        if (render) {
            getProduct()
            // setTimeout(function () {
            if (state.tabsNavigator) {
                navigation.navigate(state.tabsNavigator)
                action({
                    type: 'setTabsNavigator',
                    data: false
                })
            }
            // }, 500);
        }
    }, [render]);

    function goCategory(data) {
        navigation.navigate('Category', {
            title: data.nameCategory,
            data: data.product,
            image: data.imageCategory          
        });
    }

    async function getProduct() {
        try {
            setLoad(true)
            const data = await api('product')
            if (data) {
                setLoad(false)
                const value = data.data;
                if (value) {
                    setData(value)
                }
            }
        } catch (error) {
            setLoad(false)
            action({ type: 'errorAlert', message: error.message })
            console.log(error)
        }
    }

    return (
        <TabsContainer>
            <MainBG />
            <ScrollView
                keyboardShouldPersistTaps={'handled'}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    background: 'transparent'
                }}
            >
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 20
                }}>
                    <View>
                        <Text style={textStyle.subWelcomeLight}>{str.welcome}</Text>
                        <Text style={textStyle.welcomeLight}>{state.profile.fullName}</Text>
                    </View>
                    <TouchableOpacity>
                        <Image source={
                            state.profile.userImage ? {
                                uri: state.profile.userImage
                            } :
                                require('../../../assets/images/default-profile.png')
                        } style={{
                            height: 50,
                            width: 50,
                            resizeMode: 'cover',
                            borderRadius: 100
                        }} />
                    </TouchableOpacity>
                </View>
                <Banner render={render} />
                <View style={{
                    padding: 10,
                    borderRadius: 15,
                    marginVertical: 50,
                    marginHorizontal: 10,
                    backgroundColor: colors.light
                }}>
                    <Text style={textStyle.title}>{str.category} Salon</Text>
                    <Space size={20} />
                    {data.map((row, i) => (
                        <View key={i}>
                            <ListArticle
                                onPress={() => {
                                    goCategory(row)
                                }}
                                title={row.nameCategory}
                                content={row.descCategory}
                                icon={imageCategory(row.idCategory)}
                            />
                        </View>
                    ))}
                </View>
            </ScrollView>
        </TabsContainer>
    );
};

const Home = ({ navigation }) => {

    // const [render, setRender] = useState()

    // useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         setRender(true)
    //     });

    //     return unsubscribe;
    // }, [navigation]);

    // useEffect(() => {
    //     const unsubscribe = navigation.addListener('blur', () => {
    //         setRender(false)
    //     });

    //     return unsubscribe;
    // }, [navigation]);

    return <HomeComponet navigation={navigation}
        // render={render} 
        render={true}
    />
}

export default Home;
