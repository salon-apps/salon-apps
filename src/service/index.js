// management state using hooks and provider with react tracked help
import { useReducer } from 'react';
import { createContainer } from 'react-tracked';
const useValue = ({ reducer, initialState }) => useReducer(reducer, initialState);

export const { Provider, useTracked, useTrackedState, useUpdate } = createContainer(useValue);
export const initialState = {
    alert: {
      active: false,
      type: '',
      message: '',      
    },
    loading: false,
    locSelect: false,
    tabsNavigator: false,
    updateState: false,
    order: [],
    history: [],
    profile: {
      email: null,
      userImage: null,
      fullName: null,
      hp: null
    }
  };

export const reducer = (state, action) => {
    switch (action.type) {
      case 'errorAlert': return { ...state, alert: {active: true, type: action.snackbarType ? action.snackbarType : 'Error', message: action.message} };
      case 'successAlert': return { ...state, alert: {active: true, type: 'success', message: action.message ? action.message : 'Success' } };
      case 'closeAlert': return { ...state, alert: {...state.alert, active: false} };
      case 'loadStart': return { ...state, loading: true };
      case 'loadStop': return { ...state, loading: false };      
      case 'setHistory': return { ...state, order: action.order, history: action.history };
      case 'locSelect': return { ...state, locSelect: action.data };
      case 'setTabsNavigator': return { ...state, tabsNavigator: action.data };
      case 'updateProfile': return { ...state, profile: action.data };
      case 'updateState': return { ...state, updateState: action.data };
      default: throw new Error(`unknown action type: ${action.type}`);
    }
  };
