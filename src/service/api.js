import * as RootNavigation from '../router/root';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { ENV } from '../../env';

const header = {
    "Content-Type": "application/json",
}

async function getAuthHeader() {
    return {
        "Content-Type": "application/json",
        "Authorization": "beAuty " + await AsyncStorage.getItem('token')
    }
}

const host = ENV.host;

const getPayload = async (name, data) => {
    switch (name) {
        case 'login':
            return {
                headers: header,
                method: 'post',
                url: host + '/login',
                data
            }
        case 'loginGoogle':
            return {
                headers: header,
                method: 'post',
                url: host + '/google-login',
                data
            }
        case 'product':
            return {
                headers: header,
                method: 'get',
                url: host + '/product',
                data: null
            }
        case 'history':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/history',
                data: null
            }
        case 'order':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/order',
                data
            }
        case 'cancelOrder':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/cancel/' + data,
                data: null
            }
        case 'updateDevice':
            return {
                headers: await getAuthHeader(),
                method: 'post',
                url: host + '/update-device',
                data
            }
        case 'about':
            return {
                headers: header,
                method: 'get',
                url: host,
                data
            }
        case 'detailPartner':
            return {
                headers: await getAuthHeader(),
                method: 'get',
                url: host + '/detail-partner/' + data,
                data: null
            }
        case 'updateRating':
            return {
                headers: await getAuthHeader(),
                method: 'put',
                url: host + '/rating',
                data
            }
        case 'geocode':
            return {
                headers: header,
                method: 'post',
                url: 'https://locationiq.org/v1/reverse.php?key=' + data.key +
                    '&lat=' + data.latitude + '&lon=' + data.longitude + '&format=json',
                data: {}
            }
        default:
            throw 'Error api not found'
    }
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}


export const api = async (name, data) => {
    const payload = await getPayload(name, data)
    const req = await request(payload)
    if (req.errorCode === 'authConflict') {
        // RootNavigation.navigate('Login');
        RootNavigation.resetAll()
    }
    return req
}