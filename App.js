import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { Provider, reducer, initialState } from './src/service';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import RNBootSplash from "react-native-bootsplash";
import MainRoot from './src/router';
import { Activity } from './src/components/activity';
import FlashMessage from "react-native-flash-message";
import { colors } from './src/styles';
import textStyle from './src/styles/text';

const theme = {
  ...DefaultTheme,
  roundness: 7,
  colors: {
    ...DefaultTheme.colors,
    primary: '#5073ED',
    accent: '#f1c40f',
  },
};

function App() {

  let init = async () => {
    console.log('running debug mode ...')
  };

  useEffect(() => {    
    init().finally(() => {
      RNBootSplash.hide({ duration: 500 });
    });
  }, []);

  return (
    <Provider reducer={reducer} initialState={initialState}>
      <PaperProvider theme={theme}>
        <Activity />
        <MainRoot />
        <FlashMessage
          position={'top'}
          animated={true}
          icon={'info'}
          hideStatusBar
          duration={6000}
          style={{
            backgroundColor: colors.whatsapp
          }}
          titleStyle={[
            textStyle.title,
            {
              color: colors.light
            }
          ]}
          textStyle={[
            textStyle.subTitle,
            {
              color: colors.light
            }
          ]}
        />
      </PaperProvider>
    </Provider>
  );
}

export default App;